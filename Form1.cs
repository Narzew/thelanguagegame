﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace LangiX
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button_register_Click(object sender, EventArgs e)
        {
            RegisterForm rf = new RegisterForm();
            DialogResult dr = rf.ShowDialog();
        }

        private void button_login_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists("profiles.ncp"))
            {
                // Get login and password
                String login = text_login.Text;
                String password = text_password.Text;
                // Hash password
                password = Engine.ComputeSHA1(password);
                // Parse rest
                User current_user;
                String[] tmp_array;
                String[] user_info = System.IO.File.ReadAllLines("profiles.ncp");
                Boolean logged_in = false;
                foreach (String user_data in user_info)
                {
                    tmp_array = user_data.Split('\0');
                    if (tmp_array[0] == login && tmp_array[1] == password)
                    {
                        current_user = new User(tmp_array[0], tmp_array[2], tmp_array[3]);
                        logged_in = true;
                        SetSelect.user = current_user;
                        SetSelect.user.load_data();
                        SetSelect setselect = new SetSelect();
                        this.Hide();
                        setselect.Closed += (s, args) => this.Close();
                        setselect.Show();
                    }
                }
                if (!(logged_in))
                {
                    MessageBox.Show("Invalid login or password!", "Login failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                System.IO.File.Create("profiles.ncp");

            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (!(System.IO.File.Exists("profiles.ncp")))
            {
                System.IO.File.Create("profiles.ncp");

                // Open register dialog
                /*
                RegisterForm rf = new RegisterForm();
                DialogResult dr = rf.ShowDialog();
                 */
            }
        }
    }
}
