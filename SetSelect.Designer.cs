﻿namespace LangiX
{
    partial class SetSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.list_languages = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.list_sets = new System.Windows.Forms.ListBox();
            this.button_learn = new System.Windows.Forms.Button();
            this.button_logout = new System.Windows.Forms.Button();
            this.level_current = new System.Windows.Forms.ProgressBar();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.level_next = new System.Windows.Forms.ProgressBar();
            this.level_current_text = new System.Windows.Forms.Label();
            this.level_next_text = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // list_languages
            // 
            this.list_languages.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.list_languages.FormattingEnabled = true;
            this.list_languages.ItemHeight = 24;
            this.list_languages.Location = new System.Drawing.Point(11, 59);
            this.list_languages.Name = "list_languages";
            this.list_languages.Size = new System.Drawing.Size(282, 244);
            this.list_languages.TabIndex = 0;
            this.list_languages.SelectedIndexChanged += new System.EventHandler(this.list_languages_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(40, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(235, 35);
            this.label1.TabIndex = 10;
            this.label1.Text = "Available languages";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(359, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(167, 35);
            this.label2.TabIndex = 11;
            this.label2.Text = "Available sets";
            // 
            // list_sets
            // 
            this.list_sets.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.list_sets.FormattingEnabled = true;
            this.list_sets.ItemHeight = 24;
            this.list_sets.Location = new System.Drawing.Point(301, 59);
            this.list_sets.Name = "list_sets";
            this.list_sets.Size = new System.Drawing.Size(282, 244);
            this.list_sets.TabIndex = 12;
            this.list_sets.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.list_sets_MouseDoubleClick);
            // 
            // button_learn
            // 
            this.button_learn.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_learn.Location = new System.Drawing.Point(12, 309);
            this.button_learn.Name = "button_learn";
            this.button_learn.Size = new System.Drawing.Size(281, 73);
            this.button_learn.TabIndex = 13;
            this.button_learn.Text = "Learn this set";
            this.button_learn.UseVisualStyleBackColor = true;
            this.button_learn.Click += new System.EventHandler(this.button_learn_Click);
            // 
            // button_logout
            // 
            this.button_logout.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_logout.Location = new System.Drawing.Point(302, 309);
            this.button_logout.Name = "button_logout";
            this.button_logout.Size = new System.Drawing.Size(281, 73);
            this.button_logout.TabIndex = 16;
            this.button_logout.Text = "Logout";
            this.button_logout.UseVisualStyleBackColor = true;
            this.button_logout.Click += new System.EventHandler(this.button4_Click);
            // 
            // level_current
            // 
            this.level_current.Location = new System.Drawing.Point(203, 396);
            this.level_current.Name = "level_current";
            this.level_current.Size = new System.Drawing.Size(380, 42);
            this.level_current.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(12, 403);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(170, 35);
            this.label3.TabIndex = 18;
            this.label3.Text = "Current level:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(12, 442);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 35);
            this.label4.TabIndex = 19;
            this.label4.Text = "Next level:";
            // 
            // level_next
            // 
            this.level_next.Location = new System.Drawing.Point(203, 446);
            this.level_next.Name = "level_next";
            this.level_next.Size = new System.Drawing.Size(380, 42);
            this.level_next.TabIndex = 20;
            // 
            // level_current_text
            // 
            this.level_current_text.BackColor = System.Drawing.Color.Transparent;
            this.level_current_text.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.level_current_text.Location = new System.Drawing.Point(203, 403);
            this.level_current_text.Name = "level_current_text";
            this.level_current_text.Size = new System.Drawing.Size(380, 35);
            this.level_current_text.TabIndex = 21;
            this.level_current_text.Text = "Current level:";
            this.level_current_text.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // level_next_text
            // 
            this.level_next_text.BackColor = System.Drawing.Color.Transparent;
            this.level_next_text.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.level_next_text.Location = new System.Drawing.Point(203, 453);
            this.level_next_text.Name = "level_next_text";
            this.level_next_text.Size = new System.Drawing.Size(380, 35);
            this.level_next_text.TabIndex = 22;
            this.level_next_text.Text = "Next level:";
            this.level_next_text.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SetSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 500);
            this.Controls.Add(this.level_next_text);
            this.Controls.Add(this.level_current_text);
            this.Controls.Add(this.level_next);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.level_current);
            this.Controls.Add(this.button_logout);
            this.Controls.Add(this.button_learn);
            this.Controls.Add(this.list_sets);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.list_languages);
            this.Name = "SetSelect";
            this.Text = "Set Selection";
            this.Load += new System.EventHandler(this.SetSelect_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox list_languages;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox list_sets;
        private System.Windows.Forms.Button button_learn;
        private System.Windows.Forms.Button button_logout;
        private System.Windows.Forms.ProgressBar level_current;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ProgressBar level_next;
        private System.Windows.Forms.Label level_current_text;
        private System.Windows.Forms.Label level_next_text;
    }
}