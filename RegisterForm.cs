﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using LangiX;

namespace LangiX
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void button_register_Click(object sender, EventArgs e)
        {
            errorProvider.Clear();
            Boolean error_occured = false;
            String login = text_login.Text;
            String password = text_password.Text;
            String password2 = text_password2.Text;
            String email = text_email.Text;
            String nick = text_nick.Text;

            int login_available_res1 = Engine.check_login_available(login);
            int login_res1 = Engine.check_login(login);
            int password_res1 = Engine.check_password(password, password2);
            int email_res1 = Engine.check_email(email);
            int nick_res1 = Engine.check_nick(nick);

            // Check login
            if (login_res1 == -1)
            {
                errorProvider.SetError(text_login, "Login too short!");
                error_occured = true;
            }
            else if (login_res1 == 1)
            {
                errorProvider.SetError(text_login, "Login too long!");
                error_occured = true;
            }
            else if (login_available_res1 == 1)
            {
                errorProvider.SetError(text_login, "This user already exist!");
                error_occured = true;
            }
            // Check password
            if (password_res1 == -1)
            {
                errorProvider.SetError(text_password, "Password too short!");
                error_occured = true;
            }
            else if (password_res1 == 1)
            {
                errorProvider.SetError(text_password, "Password too long!");
                error_occured = true;
            }
            else if (password_res1 == 2)
            {
                errorProvider.SetError(text_password, "Passwords don't match!");
                errorProvider.SetError(text_password2, "Passwords don't match!");
                error_occured = true;
            }
            // Check e-mail
            if (email_res1 == -1)
            {
                errorProvider.SetError(text_email, "E-mail too short!");
                error_occured = true;
            }
            else if (email_res1 == 1)
            {
                errorProvider.SetError(text_email, "E-mail too long!");
                error_occured = true;
            }
            else if (email_res1 == 2)
            {
                errorProvider.SetError(text_email, "E-mail is incorrect!");
                error_occured = true;
            }
            // Check nick
            if (nick_res1 == -1)
            {
                errorProvider.SetError(text_nick, "Nick too short!");
                error_occured = true;
            }
            else if (nick_res1 == 1)
            {
                errorProvider.SetError(text_nick, "Nick too long!");
                error_occured = true;
            }
            if (!(error_occured))
            {
                if (!(System.IO.File.Exists("profiles.ncp")))
                {
                    System.IO.File.Create("profiles.ncp");
                }

                password = Engine.ComputeSHA1(password);
                String s = login + "\0" + password + "\0" + email + "\0" + nick + "\n";
                String users_str = System.IO.File.ReadAllText("profiles.ncp");
                System.IO.File.WriteAllText("profiles.bak", users_str);
                s = users_str+s;
                System.IO.File.WriteAllText("profiles.ncp", s);
                if (!(System.IO.Directory.Exists("users/" + login)))
                {
                    System.IO.Directory.CreateDirectory("users/" + login);
                }
                
                MessageBox.Show("Registration complete!", "Registration", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }
    }
}
