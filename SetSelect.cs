﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace LangiX
{
    public partial class SetSelect : Form
    {
        List<Pack> pack_list = new List<Pack>();
        List<String> enabled_languages = new List<String>();
        List<String> pack_codes = new List<String>();
        public static User user { get; set; }
        public static Pack pack { get; set; }
        public static int selected_level = 0;
        public static String serialize_filename = "";


        public SetSelect()
        {
            InitializeComponent();
        }

        public void enable_language(String language)
        {
            if (enabled_languages.Contains(language))
            {
                return;
            }
            else
            {
                enabled_languages.Add(language);
            }
        }

        public void loadSet()
        {
            String selected_code = pack_codes[list_sets.SelectedIndex].ToString();
            MessageBox.Show("Selected set: "+selected_code+"\nLearning mode is not implemented yet!", "Loading set", MessageBoxButtons.OK, MessageBoxIcon.Information);
            // TODO: Load level
        }

        private void SetSelect_Load(object sender, EventArgs e)
        {
            button_logout.Text = "Logout (" + user.getNick() + ")";
            //level_current_text.Parent = level_current;
            //level_next_text.Parent = level_next;

            double exp_points = SetSelect.user.getExpPoints();
            int current_level = Engine.getLevel(exp_points);
            level_current_text.Text = "Level "+current_level;
            level_current.Value = current_level;
            level_current.Maximum = 500;
            level_current.Minimum = 1;
            double percentage = Engine.getPercentage(exp_points);
            level_next.Value = Convert.ToInt32(percentage);
            level_next.Minimum = 0;
            level_next.Maximum = 100;
            level_next_text.Text = Engine.getPercentageStr(exp_points);
            level_next_text.Visible = true;
            level_current_text.Visible = true;

            int count = 0;
            String[] language_list = System.IO.File.ReadAllLines("data/languages.txt");
            Array.Sort(language_list);
            String[] line_data;
            String[] file_list = System.IO.Directory.GetFiles("sets",
            "*.*",
            SearchOption.AllDirectories);
            foreach (String file in file_list)
            {
                if (file.Contains("settings.txt")){
                    Pack tmp_pack = new Pack();
                    String[] file_contents = System.IO.File.ReadAllLines(file);
                    foreach (String line in file_contents)
                    {
                        if (line.StartsWith("code=")){
                            tmp_pack.setCode(line.Replace("code=",""));
                        }
                        else if (line.StartsWith("author="))
                        {
                            tmp_pack.setAuthor(line.Replace("author=", ""));
                        }
                        else if (line.StartsWith("author_name="))
                        {
                            tmp_pack.setAuthorName(line.Replace("author_name=", ""));
                        }
                        else if (line.StartsWith("pack_code="))
                        {
                            tmp_pack.setPackCode(line.Replace("pack_code=", ""));
                        }
                        else if (line.StartsWith("pack_name="))
                        {
                            tmp_pack.setPackName(line.Replace("pack_name=", ""));
                        }
                        else if (line.StartsWith("difficulty_min="))
                        {
                            tmp_pack.setMinDifficulty(Int32.Parse(line.Replace("difficulty_min=", "")));
                        }
                        else if (line.StartsWith("difficulty_max="))
                        {
                            tmp_pack.setMaxDifficulty(Int32.Parse(line.Replace("difficulty_max=", "")));
                        }
                        else if (line.StartsWith("language1="))
                        {
                            tmp_pack.setLanguage1(line.Replace("language1=", ""));
                            enable_language(line.Replace("language1=", ""));
                        }
                        else if (line.StartsWith("language2="))
                        {
                            tmp_pack.setLanguage2(line.Replace("language2=", ""));
                        }
                    }
                    pack_list.Add(tmp_pack);
                }
            }
            
            // Add languages code
            foreach (String line in language_list)
            {
                if (line.Contains("\t")){
                    line_data = line.Split('\t');
                    if(enabled_languages.Contains(line_data[0])){
                        list_languages.Items.Add(line_data[0]+" - " +line_data[1]);
                        count++;
                    }
                    
                }
            }

        }

        private void list_languages_SelectedIndexChanged(object sender, EventArgs e)
        {
            String selected_lang_code = list_languages.Items[list_languages.SelectedIndex].ToString();
            selected_lang_code = selected_lang_code.Replace(" - ", "\0");
            String language_code = selected_lang_code.Split('\0')[0];
            // Show packs with this langCode
            list_sets.Items.Clear();
            pack_codes.Clear();
            foreach(Pack pack_data in pack_list){
                if (pack_data.checkLanguage(language_code))
                {
                    list_sets.Items.Add(pack_data.getPackName());
                    pack_codes.Add(pack_data.getCode());
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SetSelect.user.dump_data();
            Form1 form1 = new Form1();
            this.Hide();
            form1.Closed += (s, args) => this.Close();
            form1.Show();
        }

        private void list_sets_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            loadSet();
        }



        private void button_learn_Click(object sender, EventArgs e)
        {
            loadSet();
        }


    }
}
