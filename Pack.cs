﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace LangiX
{
    public class Pack
    {
        string code = "";
        string author = "";
        string author_name = "";
        string pack_code = "";
        string pack_name = "";
        int difficulty_min = 0;
        int difficulty_max = 0;
        string language1 = "";
        string language2 = "";

        public Pack()
        {

        }

        public void setCode(String code)
        {
            this.code = code;
        }

        public void setAuthor(String author)
        {
            this.author = author;
        }

        public void setAuthorName(String authorname)
        {
            this.author_name = authorname;
        }

        public void setPackCode(String packcode)
        {
            this.pack_code = packcode;
        }

        public void setPackName(String packname)
        {
            this.pack_name = packname;
        }

        public void setLanguage1(String language1)
        {
            this.language1 = language1;
        }

        public void setLanguage2(String language2)
        {
            this.language2 = language2;
        }

        public void setMinDifficulty(int difficulty)
        {
            this.difficulty_min = difficulty;
        }

        public void setMaxDifficulty(int difficulty)
        {
            this.difficulty_max = difficulty;
        }

        public Boolean checkLanguage(String language)
        {
            if (this.language1 == language)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean checkLanguageReverse(String language)
        {
            if (this.language2 == language)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean checkLanguage(String language, String language2)
        {
            if (this.language1 == language && this.language2 == language2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean checkCode(String code)
        {
            if (this.code == code)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public String getPackName()
        {
            return this.pack_name;
        }

        public String getCode()
        {
            return this.code;
        }

        public String getPackCode()
        {
            return this.pack_code;
        }

        public String[] getPackLevels()
        {
            String level_filename = "sets/" + code + "/levels.txt";
            if (System.IO.File.Exists(level_filename))
            {
                String[] lines = System.IO.File.ReadAllLines(level_filename);
                List<String> levels = new List<String>();
                foreach (String line in lines)
                {
                    levels.Add(line.Split(';')[1]);
                }
                return levels.ToArray();
            }
            else
            {
                return null;
            }
        }

        public String[] getLevel(int level)
        {
            String level_filename = "sets/" + code + "/levels/level_" + level + ".txt";
            if (System.IO.File.Exists(level_filename))
            {
                String[] lines = System.IO.File.ReadAllLines(level_filename);
                return lines;
            }
            else
            {
                return null;
            }
        }

        public int getLevelId(String filename)
        {
            if (filename.Contains("level_") && (filename.Contains(".")))
            {
                filename = filename.Replace("level_", "\0");
                String[] parts = filename.Split('\0');
                return Int32.Parse(parts[1].Split('.')[0]);
            }
            else
            {
                return 0;
            }
        }

    }
}
