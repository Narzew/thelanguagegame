﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangiX
{
    public class User
    {
        private String login = "";
        private String email = "";
        private String nick = "";

        private double exp_points = 0;
        private double total_hits = 0;
        private double total_good = 0;

        public User(string login, String email, String nick)
        {
            this.login = login;
            this.email = email;
            this.nick = nick;
        }

        public String getLogin()
        {
            return login;
        }

        public String getEmail()
        {
            return email;
        }

        public String getNick(){
            return nick;
        }

        public int getExpPoints()
        {
            return Convert.ToInt32(this.exp_points);
        }

        public void addExp(int exp){
            this.exp_points = this.exp_points + exp;
        }

        public void addExp(double exp)
        {
            this.exp_points = this.exp_points + exp;
        }

        public void addHit(Boolean good)
        {
            this.total_hits++;
            if (good)
            {
                this.total_good++;
            }
        }

        public void dump_data()
        {
            String filename = "users/" + login + ".ncp";
            if (System.IO.File.Exists(filename))
            {
                // Do backup
                String backup_filename = "users/" + login + ".bak";
                System.IO.File.WriteAllText(backup_filename, System.IO.File.ReadAllText(filename));
            }
            // Write data
            String data = login + "\0" + email + "\0" + nick + "\0" + exp_points + "\0" + total_hits + "\0" + total_good + "\0";
            System.IO.File.WriteAllText(filename, data);
        }

        public void load_data()
        {
            String filename = "users/" + login + ".ncp";
            if (System.IO.File.Exists(filename))
            {
                String data = System.IO.File.ReadAllText(filename);
                String[] splita = data.Split('\0');
                this.login = splita[0];
                this.email = splita[1];
                this.nick = splita[2];
                this.exp_points = Convert.ToDouble(splita[3]);
                this.total_hits = Int32.Parse(splita[4]);
                this.total_good = Int32.Parse(splita[5]);
            }
        }
    }

}
